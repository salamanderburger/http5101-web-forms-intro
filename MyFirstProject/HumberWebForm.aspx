﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HumberWebForm.aspx.cs" Inherits="MyFirstProject.HumberWebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta charset="utf-8" />
    <title>My Title</title>
</head>
<body>
    <a href="https://www.google.com">My google link</a>
    <form id="form1" runat="server">
        <!-- We are about to run some server code -->
        <% 
            /*This is a comment in server code
             * Unlike HTML comments, you won't be able to see these if you inspect the document
             */
            Response.Write("hello");
            /*Server code is powerful because it can execute logic and math before the page is loaded*/
            Response.Write(1 + 2);
            %>
        <!-- 
            We use special server commands called "server controls"
            to write HTML. asp:TextBox and <input type="text"> end up being near identical when the page is loaded,
            but the process of their creation is very different.
        -->
        <label>First Name</label><asp:TextBox ID="fname" runat="server"></asp:TextBox>
        <br />
        <label>Last Name</label><asp:TextBox ID="lname" runat="server"></asp:TextBox>

        <!--
            In our next class we will be learning all of the server controls that we can use to mimic a regular HTML
            form
            -->
    </form>
</body>
</html>